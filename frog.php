<?php

    class frog{
        public $name;
        public $legs = 4;
        public $cold_blooded = "no";

        public function jump(){
            echo "Jump : hop hop";
        }

        public function __construct($name){
            $this ->name = $name;
        }
    }

?>