<?php
    require ("animal.php");

    $sheep = new animal("shaun");

    echo "Name : " . $sheep->name . "<br>";
    echo "Legs : " . $sheep->legs . "<br>";
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>";

    require ("ape.php");

    $sungokong = new ape("Kera Sakti");

    echo "Name : " . $sungokong-> name . "<br>";
    echo "Legs : " . $sungokong-> legs . "<br>";
    echo "Cold Blooded : " . $sungokong-> cold_blooded . "<br>";
    echo $sungokong-> yell() . "<br><br>";

    require ("frog.php");

    $kodok = new frog("Buduk");

    echo "Name : " . $kodok-> name . "<br>";
    echo "Legs : " . $kodok-> legs . "<br>";
    echo "Cold Blooded : " . $kodok-> cold_blooded . "<br>";
    echo $kodok-> jump() . "<br>";


?>